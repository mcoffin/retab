#![allow(dead_code)]

pub fn side_effect<F, A>(mut f: F) -> impl FnMut(A) -> A where
    F: FnMut(&A),
{
    move |v| {
        f(&v);
        v
    }
}

pub fn side_effect_mut<F, A>(mut f: F) -> impl FnMut(A) -> A where
    F: FnMut(&mut A),
{
    move |mut v| {
        f(&mut v);
        v
    }
}
