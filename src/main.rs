extern crate clap;
extern crate regex;
#[macro_use] extern crate log;
extern crate env_logger;
#[macro_use] extern crate lazy_static;

mod logging;
mod side_effect_utils;

use clap::Clap;
use regex::Regex;
use std::{
    fs,
    io,
    iter,
    path::{
        Path,
        PathBuf,
    },
    process,
};
use side_effect_utils::*;

#[derive(Debug, Clap)]
struct Config {
    #[clap(short, long, default_value = "4")]
    tabstop: u8,
    #[clap(short, long, default_value = ".retabbed")]
    output_suffix: String,
    #[clap(parse(from_os_str))]
    files: Vec<PathBuf>,
}

impl Config {
    fn validate(&self) -> io::Result<()> {
        use io::ErrorKind;
        if self.files.len() < 1 {
            return Err(io::Error::new(ErrorKind::Other, "You must specify at least one file"));
        }
        Ok(())
    }

    pub fn files<'a>(&'a self) -> impl Iterator<Item=&'a Path> {
        self.files.iter()
            .map(|b| b.as_ref())
    }

    fn tabstop_pattern(&self) -> Regex {
        Regex::new(&format!("^[ ]{}{}{}", "{", self.tabstop, "}")).unwrap()
    }

    pub fn retab<'a, R: io::BufRead + 'a>(&'a self, r: R) -> impl Iterator<Item=io::Result<String>> + 'a {
        let pattern = self.tabstop_pattern();
        r.lines().map(move |line| {
            let line = line?;
            let original_line = line.clone();
            let mut line = line.as_str();
            let mut prefix_count = 0usize;
            while line.len() > 0 && pattern.is_match(line) {
                let (_, newLine) = line.split_at(self.tabstop as usize);
                line = newLine;
                prefix_count = prefix_count + 1;
            }

            let prefix = iter::repeat("\t")
                .take(prefix_count)
                .fold(String::with_capacity(self.tabstop as usize), |mut s, v| {
                    s.push_str(v);
                    s
                });
            let retabbed = format!("{}{}", &prefix, line);
            debug!("original: {:?}", &original_line);
            debug!("retabbed: {:?}", &retabbed);
            debug!("prefix: {:?}", &prefix);
            debug!("prefix_count: {}", prefix_count);
            Ok(retabbed)
        })
    }
}

fn main() {
    logging::init();
    let config = Config::parse();
    debug!("{:?}", &config);
    if let Err(e) = config.validate() {
        error!("Invalid parameters: {}", &e);
        process::exit(1);
    }

    config
        .files()
        .map(side_effect::<_, &Path>(|&p| info!("retabbing file: {}", p.display())))
        .map(|p| (p, fs::OpenOptions::new().read(true).open(p).map(io::BufReader::new)))
        .for_each(|(p, fr)| fr.and_then(|f| {
            use io::Write;

            let mut output_file_path = PathBuf::from(p);
            if let Some(filename) = output_file_path.file_name().map(PathBuf::from) {
                output_file_path.set_file_name(format!("{}{}", filename.to_string_lossy(), &config.output_suffix));
                debug!("output file for {}: {}", p.display(), output_file_path.display());
                let mut output_file = fs::OpenOptions::new()
                    .create(true)
                    .write(true)
                    .open(output_file_path)?;
                for line in config.retab(f).map(|v| v.expect("Failed reading input file")) {
                    write!(&mut output_file, "{}\n", line)?;
                }
            } else {
                warn!("Skipping file due to bad path: {}", p.display());
            }
            Ok(())
        }).expect("retabbing failed"));
}
