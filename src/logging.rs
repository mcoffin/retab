use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

static INIT_LOGGING: Once = Once::new();
const DEFAULT_LOG_LEVEL: &'static str = "info";

fn env_or_default<K, V>(key: K, default_value: V) where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    let key = key.as_ref();
    if env::var_os(key).is_none() {
        env::set_var(key, default_value);
    }
}

pub fn init() {
    INIT_LOGGING.call_once(|| {
        env_or_default("RUST_LOG", format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        env_logger::init();
        trace!("logging initialized");
    });
}
